import nativeAjax from "./ajax.es6";
import store from "./store.es6";

export default function ajax(p) {
	done(false);
	return nativeAjax(p)
		.then(response => {
			done(true);
			return Promise.resolve(response);
		})
		.catch(e => {
			done(true);
			if (e.status === 401 || e.status === 403)
				store.commit('logout');
			else
				return Promise.reject(e);
		});
}

function done(isDone) {
	document.dispatchEvent(new Event(isDone ? 'loaded' : 'loading'));
}