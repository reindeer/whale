import Vue from "vue";
import Vuex from "vuex";
import StoreUser from "./store/user.es6";

Vue.use(Vuex);

export default new Vuex.Store({
	modules: {
		user: StoreUser,
	}
});