import User from "../user.es6";
import ajax from '../wh-ajax.es6';

export default {
	state: {
		me: {},
	},
	mutations: {
		login(state, {user}) {
			state.me = new User(user);
		},
		logout(state) {
			state.me = new User();
		},
	},
	actions: {
		async login({commit}, params) {
			let user = await ajax({url: 'login', ...params}).catch(_ => null);
			if (!user)
				return;
			commit('login', {user});
			return user;
		},
		async logout({commit}) {
			await ajax({url: 'login', method: 'DELETE'});
			commit('logout');
		}
	}
};