import Vue from "vue";
import VueMaterial from "vue-material";
import whApp from "../vue/wh-app.vue";
import store from "./store.es6";

document.addEventListener('DOMContentLoaded', e => {
	Vue.use(VueMaterial);
	Vue.material.registerTheme({
		default: {
			primary: {
				color: 'orange',
				hue: 600
			},
			accent: {
				color: 'blue',
				hue: 'A700'
			}
		},
	});

	let app = new Vue({
		el: '#app',
		store,
		components: {whApp: whApp},
	});
});