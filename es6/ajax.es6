export default function ajax(p) {
	if (typeof p === 'string')
		p = {url: p};
	let params = {
		url: null,
		credentials: 'same-origin',
		headers: {},
		...p
	};
	if (!params.url)
		return Promise.reject('Empty url');
	params.method = params.method ? params.method : params.body ? 'POST' : 'GET';
	params.headers = new Headers(params.headers);
	if (params.body && !(params.body instanceof FormData)) {
		params.headers.append("Content-type", "application/x-www-form-urlencoded");
		if (typeof params.body === 'object') {
			let addParamToRequest = (key, value) => key + '=' + encodeURIComponent(value);
			params.body = Object.keys(params.body).reduce((p, c) => {
				let array = params.body[c] instanceof Array ? params.body[c] : [params.body[c]];
				array.forEach(value => p.push(addParamToRequest(c, value)));
				return p;
			}, [])
				.join('&');
		}
	}
	return fetch(params.url, params)
		.then(response => Promise[(response.status >= 200 && response.status < 300) ? 'resolve' : 'reject'](response))
		.then(response => response.json())
		.catch(response => Promise.reject({status: response.status || 0}));
}