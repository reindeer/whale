export default class CSV {
	constructor(text) {
		text = text.replace(/\n*$/, "");
		let lines = text.split('\n');
		let csv = {};
		for (let line of lines) {
			let [url, title, description, tag] = line.split(';');
            let match = url.match(new RegExp("http://club.mmkc.su/file/(\\d+)"));
            if (!match || !match[1])
				continue;
            let id = match[1];
			let tags = [...new Set(tag.split(','))];
			csv[id] = {
				id,
				url,
				title,
				description,
				tags
			};
		}
		this.csv = Object.values(csv);
	}
}