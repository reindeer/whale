export default class User {
	constructor(params = {}) {
		this.perms = 0;
		Object.keys(params).forEach((key => this[key] = params[key]));
	}

	static get roles() {
		return {
			admin: 128,
			topManager: 4,
			manager: 2,
			user: 1
		};
	}

	get isLogged() {return this.perms > 0;}

	get isAdmin() {return this.is(User.roles.admin);}

	get isManagement() {return this.is(User.roles.topManager) && this.is(User.roles.manager);}

	get isTopManager() {return this.is(User.roles.topManager);}

	get isManager() {return this.is(User.roles.manager);}

	get isSeller() {return this.is(User.roles.user);}

	get hasAdmin() {return this.has(User.roles.admin);}

	get hasTopManager() {return this.has(User.roles.topManager);}

	get hasManager() {return this.has(User.roles.manager);}

	get hasSeller() {return this.has(User.roles.user);}

	is(role) {return !!(this.perms & role);}

	has(role) {return this.perms >= role;}

	greater(role) {return this.perms > role;}
}