const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const babelLoader = {
	loader: 'babel-loader',
	options: {
		presets: [['env', {targets: {browsers: ["ie >= 10"]}}]],
		plugins: ['transform-object-rest-spread']
	}
};

const pcssExtractor = new ExtractTextPlugin('../css/app.css');

const pcssLoader = pcssExtractor.extract({
	fallback: 'style-loader',
	use: [
		{loader: 'postcss-loader', options: {sourceMap: true}}
	],
});

module.exports = {
	entry: {
		vendor: ['babel-polyfill', 'whatwg-fetch'],
		app: './index.js',
	},
	output: {
		path: __dirname + '/public/js',
		filename: '[name].js',
		sourceMapFilename: '[name].js.map'
	},
	resolve: {
		alias: {
			vue: 'vue/dist/vue.js'
		}
	},
	//devtool: '#inline-source-map',
	module: {
		rules: [
			{
				test: /\.es6$/,
				use: babelLoader
			},
			{
				test: /\.p?css$/,
				use: pcssLoader
			},
			{
				test: /\.vue$/,
				use: {
					loader: 'vue-loader',
					options: {
						loaders: {
							js: babelLoader,
							postcss: pcssLoader,
						}
					}
				}
			}
		]
	},
	plugins: [
		pcssExtractor,
		new webpack.DefinePlugin({
			'process.env': {NODE_ENV: JSON.stringify('production')}
		}),
		new UglifyJsPlugin(),
	]
};