module.exports = {
	plugins: {
		'postcss-import-pcss': {},
		'postcss-mixins': {},
		'postcss-cssnext': {
			browsers: ['> 2%']
		},
		'css-mqpacker': {},
		'cssnano': {},
	}
};