<?php

namespace App\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;

class Updater {
	protected $client;
	protected $url;
	protected $result;

	public function __construct($url, $login) {
		$this->client = new Client();
		$this->url = $url;
		$this->login = $login;
	}

	public function update($files) {
		$this->result = [
			'success' => [],
			'errors' => [],
		];
		foreach ($files as $file) {
			$this->updateFile($file);
			usleep(250000);
		}
		return $this->result;
	}

	protected function updateFile($file) {
		$query = $this->getQuery($file);
		if (!count($query))
			return;
		try {
			$response = $this->client->request('POST', $this->url . $file['id'], [
				'auth' => [$this->login, ''],
				'form_params' => $query,
			]);
			if ($response->getStatusCode() == 200)
				array_push($this->result['success'], $file['id']);
			else
				throw new ServerException('', $response);
		} catch (GuzzleException $e) {
			array_push($this->result['errors'], $file['id']);
		}
	}

	protected function getQuery($file) {
		$query = [];
		foreach (['title', 'description', 'tags'] as $field) {
			$value = $file[$field];
			if (empty($value))
				continue;
			if (is_array($value))
				$value = implode(',', array_map('trim', $value));
			$query[$field] = trim($value);
		}
		return $query;
	}
}