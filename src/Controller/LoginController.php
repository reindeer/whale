<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class LoginController extends Controller {
	public function check(Request $request) {
		$user = $request->getSession()->get('user');
		if (!$user)
			throw new UnauthorizedHttpException('');
		return $this->json($user);
	}

	public function login(Request $request) {
		$login = $request->get('login');
		$password = $request->get('password');
		if (empty($login) || empty($password))
			throw new BadRequestHttpException();
		if (sha1("$login:$password") != getenv('AUTH_TOKEN'))
			throw new UnauthorizedHttpException('');
		$user = ['perms' => 128];
		$request->getSession()->set('user', $user);
		return $this->json($user);
	}

	public function logout(Request $request) {
		$user = $request->getSession()->get('user');
		if (!$user)
			throw new AccessDeniedHttpException();
		$request->getSession()->set('user', null);
		return $this->json([]);
	}
}