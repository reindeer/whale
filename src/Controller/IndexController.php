<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class IndexController extends Controller {
	public function index() {
		return $this->file('index.html', '', ResponseHeaderBag::DISPOSITION_INLINE);
	}
}