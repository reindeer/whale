<?php

namespace App\Controller;

use App\Service\Updater;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class UpdateController extends Controller {
	public function update(Request $request) {
		if (!$request->getSession()->get('user'))
			throw new AccessDeniedHttpException();
		$updater = new Updater(getenv('CLUB_URL'), getenv('CLUB_TOKEN'));
		$files = json_decode($request->get('files'), true);
		$result = $updater->update($files);
		return $this->json($result);
	}
}